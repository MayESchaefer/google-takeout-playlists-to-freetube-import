<?PHP
function guid(string $seed) {
	$seed = crc32($seed);
	mt_srand($seed);
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}
function readcsv(string $filename, int $start = 0, int $end = null) {
	$file = file($filename);
	while (end($file) === "" || end($file) === "\n")
	{
		array_pop($file);
	}
	$rows = array_slice(
		array_map('str_getcsv', $file),
		$start,
		$end
	);
	$header = array_shift($rows);
	$csv = array();
	foreach ($rows as $row) {
		$csv[] = array_combine($header, $row);
	}
	return $csv;
}
function array_find(array $array, callable $function) {
	foreach ($array as $value) {
		if (call_user_func($function, $value) === true) {
			return $value;
		}
	}
	return null;
}
function strtotimestamp(string $timestamp) {
	return str_pad(
		strval(
			strtotime($timestamp)
		),
		13,
		"0",
		STR_PAD_RIGHT
	);
}
function parseyoutubevideolength(string $string) {
	$pattern = '/PT(\d+)M(\d+)S/';
	if (preg_match($pattern, $string, $matches)) {
		return strval((int)($matches[0]) * 60 + (int)($matches[1]));
	}
}
function better_similar_text(string $string1, string $string2) {
	$similarity = similar_text($string1, $string2);
	if (abs(strlen($string1) - strlen($string2)) === 0) {
		$similarity += 1;
	}
	return $similarity;
}
function getclosestmeta(string $filename, array $metadata) {
	$out = null;
	$filename = preg_replace('/-videos$/', '', pathinfo($filename, PATHINFO_FILENAME));

	foreach ($metadata as $playlist) {
		if ($out === null
			|| better_similar_text($playlist["Playlist Title (Original)"], $filename)
				> better_similar_text($out["Playlist Title (Original)"], $filename)
		) {
			$out = $playlist;
		}
	}
	return $out;
}
function getvideodata(array $video_ids, string $key) {
	$endpointuri = "https://content-youtube.googleapis.com/youtube/v3/videos";
	$parts = "part=snippet&part=contentDetails";
	array_walk($video_ids, function(&$value, $key) {
		$value = "id=" . $value;
	});
	$ids = implode("&", $video_ids);
	return json_decode(
		file_get_contents("{$endpointuri}?{$ids}&{$parts}&key={$key}")
	)->items;
}
function getvideodatachunked(array $video_ids, string $key, int $chunk_size) {
	$chunks = array_chunk($video_ids, $chunk_size);
	$data = [];
	foreach ($chunks as $chunk) {
		$chunkdata = getvideodata($chunk, $key);
		$data = array_merge($data, $chunkdata);
	}
	return $data;
}
function parseplaylist(string $playlistpath, string $apikey, array $metadata) {
	$playlistcsv = getclosestmeta($playlistpath, $metadata);
	$videocsv = readcsv($playlistpath);
	$videoids = array_map(function($value) {
		return trim($value["Video ID"]);
	}, $videocsv);
	$playlistdata = (object)[
		"playlistName" => $playlistcsv["Playlist Title (Original)"],
		"protected" => false,
		"description" => "",
		"videos" => [],
		"_id" => $playlistcsv["Playlist ID"],
		"createdAt" => strtotimestamp($playlistcsv["Playlist Create Timestamp"]),
		"lastUpdatedAt" => strtotimestamp($playlistcsv["Playlist Update Timestamp"])
	];
	$playlistdata->videos = [];

	$videodata = getvideodatachunked($videoids, $apikey, 50);

	foreach ($videodata as $videodatum) {
		$GLOBALS["videoid"] = $videodatum->id;
		$video = (object)[
			"videoId" => $videodatum->id,
			"title" => $videodatum->snippet->title,
			"author" => $videodatum->snippet->channelTitle,
			"authorId" => $videodatum->snippet->channelId,
			"lengthSeconds" => parseyoutubevideolength($videodatum->contentDetails->duration),
			"timeAdded" => strtotimestamp(
				array_find($videocsv, function($val) {
					return trim($val["Video ID"]) === $GLOBALS["videoid"];
				})["Playlist Video Creation Timestamp"]
			),
			"playlistItemId" => guid($videodatum->etag),
			"type" => "video"
		];
		array_push($playlistdata->videos, $video);
	}

	return json_encode($playlistdata);
}

$path = $argv[1];
$apikey = $argv[2];
$metapath = $path;
if (!str_ends_with($metapath, DIRECTORY_SEPARATOR)) {
	$metapath .= DIRECTORY_SEPARATOR;
}

$playlistmeta = readcsv($metapath . DIRECTORY_SEPARATOR . "playlists.csv");

$playlists = [];

$files = new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator(
		$path,
		FilesystemIterator::SKIP_DOTS
	)
);
foreach($files as $file) {
	if ($file->getFilename() !== "playlists.csv") {
		$playlist = parseplaylist((string)$file, $apikey, $playlistmeta);
		array_push($playlists, $playlist);
	}
}

$out = implode("\n", $playlists) . "\n";
file_put_contents("takeout-playlists.db", $out);
?>