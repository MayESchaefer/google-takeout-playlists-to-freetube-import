# Google Takeout Playlists To Freetube Import

Parses Google Takeout csv data and creates a .db import file for Freetube to import.

You will need a [Google API key](https://support.google.com/googleapi/answer/6158862) with access to the Youtube Data API.

Example of use:
```
php takeout_playlists_to_freetube.php "FOLDER_WITH_TAKEOUT_PLAYLISTS" "YOUR_API_KEY"
```



Fair warning, I didn't put basically any error handling in this. It's one evening's work, and like 100 lines of code. Still, it *works*.